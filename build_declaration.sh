#!/bin/bash

# Ce script récupère la liste des PVC définis dans le cluster ayant un label type=data-bynet
# Puis il utilise un fichier de rsync-resif-k8s_template.yaml pour construire la description
# complète permettant de déployer le service
# En sortie : un fichier yaml pour kubernetes
# Usage: build_declaration | kubectl apply -

# Liste de PVC
pvcs=$(kubectl get persistentvolumeclaim -ltype=data-bynet -o json | jq '.items[]|.metadata.name'|sed -e 's/"//g')

for pvc in $pvcs; do
    net=$(echo $pvc | awk -F'-' '{print toupper($3)}')
    volumename=vol-${pvc%-claim}
    archiveVolumeMounts=$archiveVolumeMounts"
            - name: $volumename
              mountPath: /archive/data/bynet/$net
              readOnly: true"
    archiveVolumes=$archiveVolumes"
        - name: $volumename
          persistentVolumeClaim:
            claimName: $pvc"
done
export archiveVolumeMounts archiveVolumes

cat rsync-resif-k8s_template.yaml | envsubst

