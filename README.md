# Service rsync.resif.fr

Ce service utilise beaucoup de volumes qui doivent être définis au préalable.

La définition des PersistentVolumes et PersistentVolumeClaims se trouve dans le projet `storage-summer-k8s`.

Une fois tous les volumes définis, on doit générer le fichier de déclaration `rsync-resif.yaml`

    build_declaration.sh | kubectl apply -


# Notes

Ce service est assez spécifique car :

  * il accède à de nombreux volumes NFS en écriture
  * il doit être accessible depuis l'extérieur sur le port rsync standard `873`
  * il doit voir les ip des clients pour gérer les permissions
  
Pour Kubernetes cela signifie :
  * faire tourner une seule instance de ce pod
  * utiliser `nginx-ingress` comme proxy TCP comme indiqué ici: https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/

Je n'ai pas réussi à mettre en œuvre ce système, et donc je me suis rabattu sur la stratégie suivante :

  * Mettre un label `services=rsync` sur un des workers
  * Configurer le Pod pour qu'il préfère tourner sur ce worker, en `hostPort: 873`

